"Protect This House" by Iori Branford

The story description is "A righteous kill opens a door to destruction."
The story creation year is 2015.

Include Variable Time Control by Eric Eve.
Include Remembering by Aaron Reed.
Include Basic Screen Effects by Emily Short.

Release along with an interpreter.

Section A - World

A bedroom is a kind of room.

The house is a region. In it are
	the home theater, the garage,
	the dining room, the hall, the music room,
	the living room, the kitchen,
	the study, the upstairs, the guest bedroom,
	the master bedroom, the bathroom, and the closet.

The garage door is a door. It is south of the driveway and north of the garage.
The front step is southwest of the driveway.
The front door is a door. It is south of the front step and north of the hall.
The hall is southwest of the garage.
The home theater is northwest of the hall.
The dining room is west of the hall.
The music room is east of the hall.
The kitchen is south of the hall.
The living room is west of the kitchen.

The upstairs is up from the hall.
The study is west of the upstairs.
The guest bedroom is a bedroom. It is east of the upstairs.
The master bedroom is a bedroom. It is south of the upstairs.
The bathroom is east of the master bedroom.
The closet is north of the bathroom.

The office is a region. In it are
	the parking lot,
	the lobby,
	the break room, and
	the desk.

The road to work is west of the driveway.
Before going to the road to work:
	if the player is not in a vehicle,
		instead say "The road was much too long to walk."

The parking lot is west of the road to work.
The lobby is west of the parking lot.
The break room is west of the lobby.
The desk is south of the break room.

Section B - Things

Ammo is a kind of value. 1 round (singular) specifies an ammo. 2 rounds (plural) specifies an ammo.

The smartphone is a thing. Understand "phone" or "cell" or "cellphone" as the smartphone.

The safe is an openable closed container. It is fixed in place in the master bedroom.

The gun is a container with carrying capacity 1. Understand "handgun", "pistol", "weapon", or "firearm" as the gun.
Definition: the gun is empty if the magazine is not in it, or the ammo of the magazine is 0 round.
Instead of inserting a thing which is not the magazine into the gun:
	say "The only thing that would fit into my gun was its magazine."

The magazine is a thing. Understand "clip" or "mag" as the magazine.
The capacity of the magazine is an ammo that varies. The capacity of the magazine is 10 rounds.
The ammo of the magazine is an ammo that varies. The ammo of the magazine is 10 rounds.
The description of the magazine is "[if the magazine is in the gun]I would have to eject the magazine to count the rounds in it.[otherwise]Through the side window I counted [ammo of the magazine] in it."

The jewelry is a thing.

The ammo box is a thing.
The ammo of the ammo box is an ammo that varies. The ammo of the ammo box is 50 rounds.
The description of the ammo box is "The ammo box held [ammo of the ammo box]."

The extra blanket is a thing.

The car is a vehicle.
A car blocker is a kind of room. The hall and lobby are car blockers.
Instead of going to a car blocker (called CB) by the car,
	say "The car wouldn't fit into the [CB]."

The taxi is a vehicle.

Section C - Actions

Shooting is an action applying to one thing. Understand "shoot [something]", "shoot at [something]", "fire at [something]", or "attack [something] with gun" as shooting.
The shooting action has a truth state called shot fired.
The shooting action has a truth state called shot hit.
Check shooting:
	if the player is not carrying the gun:
		instead say "I didn't have any weapon to fire at [the noun]."
Carry out shooting:
	now shot fired is whether or not the gun is not empty;
	if shot fired is true:
		decrease the ammo of the magazine by 1 round;
		if the noun is the actor:
			now shot hit is true;
		otherwise:
			now shot hit is a random truth state;
	otherwise:
		now shot hit is false.
Carry out someone shooting:
	now shot fired is true;
	now shot hit is a random truth state.
Report an actor shooting:
	if shot fired is true:
		say "[The actor] fired a round[if shot hit is true] into[otherwise], just missing[end if] [the noun].";
	otherwise:
		say "[The actor] clicked harmlessly at [the noun]."
After an actor shooting the player:
	if shot hit is true:
		if the actor is real:
			say "A hollow-point ballooned inside me, ruining my vital organs and plunging me into the cold void.";
			end the story;
	otherwise:
		say "A hollow-point flew past me."

Reloading is an action applying to one thing. Understand "reload [something]" or "load [something]" as reloading.
Check reloading:
	if the noun is:
		-- the gun:
			if the magazine is in the noun:
				instead say "The magazine was already in [the noun].";
			if the player is not carrying the magazine:
				instead say "I didn't have a magazine to load into [the noun].";
		-- the magazine:
			if the magazine is in the gun:
				instead say "I would have to eject the magazine before I could fill it with rounds.";
			if the ammo of the magazine is the capacity of the magazine:
				instead say "I couldn't fit another round into [the noun]. It was already full.";
			if the player is not carrying the ammo box:
				instead say "I would need to find rounds to load into [the noun].";
			if the ammo of the ammo box is 0 round:
				instead say "The ammo box had no rounds for [the noun].";
		-- otherwise:
			instead say "[The noun] wasn't something to be loaded."
Carry out reloading:
	if the noun is:
		-- the gun:
			try inserting the magazine into the gun;
		-- the magazine:
			decrease the ammo of the ammo box by 1 round;
			increase the ammo of the magazine by 1 round.
Carry out the burglar reloading:
	now the ammo of the burglar is 6 rounds.
Report reloading:
	if the noun is:
		-- the magazine:
			say "I pulled a round from the ammo box and fitted it into [the noun]."
Report someone reloading:
	say "[The actor] ejected the spent magazine and slammed in a fresh one."

Calling is an action applying to one thing. Understand "call [something]" or "dial [something]" as calling.
Check calling:
	if the player is not carrying the smartphone:
		instead say "I didn't have my phone."
	[else if the noun is:]
[Carry out calling:
Report calling:]

Section D - Characters

A person can be real or imaginary. A person is usually real.
A person can be alive or dead. A person is usually alive.
A person can be awake, sleeping well, or sleeping poorly. A person is usually awake.

The burglar is a man. Understand "thief", "criminal", "perp", "bad guy", or "robber" as the burglar.
Awareness is a kind of value. The awarenesses are unaware and aware.
The awareness of the burglar is an awareness that varies. The awareness of the burglar is unaware.
The aggression of the burglar is a number that varies. The aggression of the burglar is 0.
The ammo of the burglar is an ammo that varies. The ammo of the burglar is 6 rounds.
The wounds of the burglar is a number that varies. The wounds of the burglar is 0.
Before the burglar shooting:
	if the ammo of the burglar is 0 round:
		instead try the burglar reloading the burglar.
Carry out the burglar shooting:
	decrease the ammo of the burglar by 1 round.
Instead of attacking the burglar:
	if the player is carrying the gun:
		try shooting the burglar;
	else if the burglar is alive:
		now the awareness of the burglar is aware;
		now the aggression of the burglar is 10;
		say "He heard my charge and turned to face me. I wasn't even within two feet when his gun flashed.";
		try the burglar shooting the player;
	else:
		increase the wounds of the burglar by 1;
		say "I dealt my dead enemy a good hard [one of]punch[or]kick[cycling] just to be sure."
Before shooting the burglar:
	if the awareness of the burglar is aware:
		now the aggression of the burglar is 10;
		say "He was ready for me, and fired his own gun first.";
		try the burglar shooting the player.
After shooting the burglar:
	if shot fired is true:
		let flee-room be a random adjacent room in the house;
		let flee-direction be the best route from the location to flee-room;
		if shot hit is true:
			increase the wounds of the burglar by 1;
			say "I fired a round into the burglar. [if the burglar is alive]He fell to the floor.";
			now the burglar is dead;
			now the awareness of the burglar is unaware;
			now the aggression of the burglar is 0;
		else:
			say "I fired a round, narrowly missing the burglar.";
		if the burglar is alive:
			say "He fled [flee-direction] into the [flee-room].";
			try silently the burglar going flee-direction;
			now the awareness of the burglar is aware;
			now the aggression of the burglar is 10;
	else:
		continue the action.
Before doing something other than giving, shooting, or attacking in the presence of the burglar when the burglar is alive and the awareness of the burglar is aware:
	if the aggression of the burglar is at least 10:
		say "The burglar's patience had run out. He fired.";
		try the burglar shooting the player;
	else:
		increase the aggression of the burglar by a random number from 2 to 3.
Every turn when the burglar is alive:
	let go-direction be nothing;
	if the awareness of the burglar is aware:
		if the burglar is in a room in the house that is not the location:
			now go-direction is the best route from the location of the burglar to the location;
	else:
		if the burglar is in the location:
			now the awareness of the burglar is aware;
			say "The burglar spotted me.";
		else:
			let target-room be a random room in the house;
			now go-direction is the best route from the location of the burglar to target-room;
	if go-direction is a direction:
		try the burglar going go-direction;
		if the burglar is not in the location:
			let burglar-direction be the best route from the location to the location of the burglar;
			say "I could hear [if the awareness of the burglar is aware]him coming after me[otherwise]movement[end if] from [the burglar-direction]."

The old man is a man.
Stance is a kind of value. The stances are standing, sitting, and lying prone.
The old man's stance is a stance that varies. The old man's stance is standing.
The description of the old man is "He was [old man's stance] in the [location of the old man][if location of the old man is the driveway], right in the way of my car[end if]."

The taxi driver is a man. He is in the taxi.

Section E - Story

The burglary is a scene.
The old man's visit is a scene.
The taxi ride is a scene. [You take a taxi to work to avoid running over the old man; the taxi driver expresses disdain at your failing to drive an old man off]
The car vandalism is a scene. [Coming home you had to park the car outside; someone (the old man?) vandalized it while you were sleeping]
The road accident is a scene. [Distracted by thoughts you crash into someone or something]
The police response is a scene. [Police arrive but the old man flees; you only get one of this]
The story of the burglar is a scene. [The old man loudly tells the burglar's life story keeping you and your neighbors awake]
The wife's threat is a scene. [The wife takes the gun and tries to threaten the old man into leaving]
The job loss is a scene. [You unwittingly bring the gun to work; if anyone finds it you are fired]
The police encounter is a scene. [You bump into police while illegally carrying the gun outside of home; if caught you are fined and jailed (which is game over)]
The neighbors' abuse is a scene. [The neighbors heap excessive abuse on the old man]
The nightmare is a scene. [Repeat of the burglary with minor surrealism such as the burglar teleporting]
The wife leaving is a scene. [The wife packs up and leaves you living alone with the old man]
The deep nightmare is a scene. [More surreal nightmare where the rooms are shuffled and the burglar has a few extra lives, also if you die in this one you die for real (by shooting yourself)]
The hallucination is a scene. [You have a vision of the burglar where there's nobody, or in place of someone else; nevertheless he can shoot and kill you for real (again actually you shooting yourself) until you shoot at him (actually an innocent person)]
[Also from this point the gun and ammo appear in semi-random places, and once you come across them you automatically pick them up and can never drop them]
The mass hallucination is a scene. [Multiple hallucinations in rapid succession]

When play begins:
	now seconds_per_turn is 6;
	now the right hand status line is "[time of day]";
	now the story viewpoint is first person singular;
	now the story tense is past tense.

Chapter 1 - Burglary

The player is in the master bedroom.

The burglary begins when play begins.
When the burglary begins:
	now the time of day is 2:22 AM;
	now the ammo box is in the safe;
	now the magazine is in the safe;
	now the gun is in the safe;
	now the car is in the garage;
	now the burglar is in the living room.
The burglary ends when the player is not awake.

Before going from the garage to the driveway by the car:
	if the burglar is in any room in the house:
		instead say "I was only _hiding_ in the car. I wasn't about to leave my house to him."

Instead of sleeping:
	if the burglar is alive:
		say "If I slept now, [if the burglar is seen]the burglar[otherwise]whoever was here[end if] would make sure I never woke up again.";
	else if the time of day is after 7 AM and the time of day is before 9 PM:
		say "It wasn't my bedtime yet.";
	else if the location is a bedroom:
		now the player is sleeping well;
		say "Exhausted from my ordeal, I tucked myself in and fell right to sleep.";
	else:
		now the player is sleeping poorly;
		say "Too tired to even find a proper bed, I lay right there and made myself as comfortable as I could."

When the burglary ends:
	now the burglar is nowhere;
	pause the game.

Chapter 2 - Old Man's Visit

The old man's visit begins when the burglary ends.
When the old man's visit begins:
	now the player is awake;
	now the old man is in the driveway;
	now the taxi is in the road to work;
	now the time of day is 7:00 AM.
The old man's visit ends when the old man is not in the driveway.

After going from somewhere (called old-place) to somewhere (called new-place) by the car:
	if new-place is the location of the old man:
		now the old man's stance is lying prone;
		say "The old man slowly lay down in the way of my car."

Before going by the car in the presence of the old man:
	if the old man's stance is lying prone:
		say "I heard a crunch and a muffled groan. I threw myself out of the car and the sight of him drained the life and warmth out of me.";
		say "With my left rear wheel I had caved in his chest. He smiled a winning smile, flashing perfect white teeth for an instant before staining them with his dying cough.";
		end the story;
	otherwise:
		say "Despite the old man's efforts I manoeuvered the car around him. Briefly I glimpsed his resentful gaze in my rear-view mirror."

Instead of giving the blanket to the old man:
	now the old man is carrying the blanket;
	if the old man is awake:
		try silently the old man dropping the blanket;
	if the old man's stance is:
		-- lying prone:
			say "I lay the blanket on the old man. [if the old man is awake]He pushed it off to one side, determined to freeze to death.";
		-- otherwise:
			say "I draped the blanket over the old man's shoulders. [if the old man is awake]He shrugged and let it fall to the ground behind him."

Instead of putting the blanket on the old man, try giving the blanket to the old man.